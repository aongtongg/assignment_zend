<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Db\Adapter\Adapter;

/*
$this->params()->fromPost('paramname');   // From POST
$this->params()->fromQuery('paramname');  // From GET
$this->params()->fromRoute('paramname');  // From RouteMatch
$this->params()->fromHeader('paramname'); // From header
$this->params()->fromFiles('paramname');
*/
class TestController extends AbstractActionController
{
    ################################################################################
    public function __construct()
    {
        $this->cacheTime = 36000;
        $this->now = date('Y-m-d H:i:s');
        $this->config = include __DIR__.'../../../../config/module.config.php';
        $this->adapter = new Adapter($this->config['Db']);
    }
    ################################################################################
    public function basic()
    {
        $view = new ViewModel();
        //Route
        $view->action = $this->params()->fromRoute('action', 'index');

        return $view;
    }
    ################################################################################
    public function indexAction()
    {
        try {
            $view = $this->basic();
            $act = $this->params()->fromQuery('act', '');
            if ($act == 'question1') {
                $question = $this->params()->fromPost('question');
                if ($question) {
                    $view->question1 = $question;
                    $view->anwser1 = $this->question1($question);
                }
            } elseif ($act == 'question2') {
                $question = $this->params()->fromPost('question');
                if ($question) {
                    $view->question2 = $question;
                    $view->anwser2 = $this->question2($question);
                }
            } elseif ($act == 'question3') {
                $question = $this->params()->fromPost('question');
                if ($question) {
                    $view->question3 = $question;
                    $view->anwser3 = $this->question3($question);
                }
            }

            return $view;
        } catch (Exception $e) {
            print_r($e);
        }
    }
    ################################################################################
    private function question1($question)
    {
        try {
            static $_cache = array();

            $result = 'Invalid pattern';
            $question = preg_replace('/\s+/', '', $question);
            if (!array_key_exists($question, $_cache)) {
                if ($question) {
                    $listNumbers = explode(',', $question);
                    $lastString = array_pop($listNumbers);
                    $countList = count($listNumbers);
                    if ($countList > 1) {
                        if ($lastString == 'X') {
                            $increase = $this->question1_solve($listNumbers);
                            if ($increase) {
                                $result = end($listNumbers) + $increase;
                            }
                        }
                    }
                }
                $_cache[$question] = $result;
            }

            return $_cache[$question];
        } catch (Exception $e) {
            print_r($e);
        }
    }
    ################################################################################
    private function question1_solve($listNumbers, $round = 1)
    {
        try {
            if (is_array($listNumbers) && !empty($listNumbers) && $round < 3) {
                if (count($listNumbers) == 1) {
                    return $listNumbers[0];
                } else {
                    $increase = array();
                    $i = 0;
                    $sameIncrease = true;
                    foreach ($listNumbers as $key => $value) {
                        if ($key) {
                            array_push($increase, $value - $listNumbers[$key - 1]);
                            if ($i && $increase[$i] != $increase[$i - 1]) {
                                $sameIncrease = false;
                            }
                            ++$i;
                        }
                    }
                    if ($sameIncrease) {
                        return $increase[0];
                    } else {
                        $solve = $this->question1_solve($increase, ++$round);
                        if ($solve === false) {
                            return false;
                        } else {
                            return end($increase) + $solve;
                        }
                    }
                }
            } else {
                return false;
            }
        } catch (Exception $e) {
            print_r($e);
        }
    }
    ################################################################################
    private function question2($question)
    {
        try {
            static $_cache = array();

            $result = 'Invalid pattern';
            $question = preg_replace('/\s+/', '', $question);
            if (!array_key_exists($question, $_cache)) {
                if ($question) {
                    $regex = '/^\([Y]([\+\-\*\/\x{00D7}\x{00F7}])(\d+)\)([\+\-\*\/\x{00D7}\x{00F7}])\((\d+)([\+\-\*\/\x{00D7}\x{00F7}])(\d+).=(\d+)$/u';
                    preg_match($regex, $question, $matches);
                    if (!empty($matches)) {
                        $result = $this->question2_solve($matches[4], $matches[6], $matches[5]);
                        $result = $this->question2_solve($matches[7], $result, $matches[3], true);
                        $result = $this->question2_solve($result, $matches[2], $matches[1], true);
                    }
                }
                $_cache[$question] = $result;
            }

            return $_cache[$question];
        } catch (Exception $e) {
            print_r($e);
        }
    }
    ################################################################################
    private function question2_solve($x, $y, $operator, $reverse = false)
    {
        try {
            static $_cache = array();

            $key = $x.'-'.$y.'-'.$operator.'-'.$reverse;
            if (!array_key_exists($key, $_cache)) {
                if ($operator == '+') {
                    if ($reverse) {
                        $result = $x - $y;
                    } else {
                        $result = $x + $y;
                    }
                } elseif ($operator == '-') {
                    if ($reverse) {
                        $result = $x + $y;
                    } else {
                        $result = $x - $y;
                    }
                } elseif ($operator == '*' || $operator == '×') {
                    if ($reverse) {
                        $result = $x / $y;
                    } else {
                        $result = $x * $y;
                    }
                } elseif ($operator == '/' || $operator == '÷') {
                    if ($reverse) {
                        $result = $x * $y;
                    } else {
                        $result = $x / $y;
                    }
                } else {
                    $result = 0;
                }
                $_cache[$key] = $result;
            }

            return $_cache[$key];
        } catch (Exception $e) {
            print_r($e);
        }
    }
    ################################################################################
    private function question3($question)
    {
        try {
            static $_cache = array();

            $result = 'Invalid pattern';
            $question = preg_replace('/\s+/', '', $question);
            if (!array_key_exists($question, $_cache)) {
                if ($question) {
                    $regex = '/^If(.*?)Then(\d+)=X$/';
                    preg_match($regex, $question, $matches);
                    if (!empty($matches)) {
                        $lists = explode(',', $matches[1]);
                        $invalid = false;
                        $i = 1;
                        $concat = '';
                        foreach ($lists as $key => $value) {
                            $regex = '/^(\d+)=(\d+)$/';
                            preg_match($regex, $value, $matches2);
                            if (empty($matches2)) {
                                $invalid = true;
                                break;
                            } else {
                                if ($i == $matches2[1]) {
                                    if ($i == 1) {
                                        $concat = $matches2[2];
                                    } else {
                                        $concat = $matches2[1].$concat;
                                    }
                                    if ($concat != $matches2[2]) {
                                        $invalid = true;
                                        break;
                                    }
                                    ++$i;
                                } else {
                                    $invalid = true;
                                    break;
                                }
                            }
                        }

                        if (!$invalid && $i == $matches[2]) {
                            $result = $matches[2].$concat;
                        }
                    }
                }
                $_cache[$question] = $result;
            }

            return $_cache[$question];
        } catch (Exception $e) {
            print_r($e);
        }
    }
}
